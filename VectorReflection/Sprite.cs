﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace VectorReflection
{
	public class Sprite
	{
		private readonly Color _color;
		private Vector2 _velocity;
	    private float _angle;
	    private float _rotationSpeed;
	    private Vector2 _scale;
		private Vector2 _position;
		private Rectangle _rectangle;
		private Texture2D _texture;
		private Rectangle? _bounds;
	    private Vector2 _origin;
	    private Matrix _transform;
	    private float _scaleValue;

		public Sprite(Vector2 position, float speed = 0, float angle = 0, float rotationSpeed = 0, float scale = 1.0f, 
            Rectangle? bounds = null)
		{
			_position = position;
		    _angle = angle;
		    _rotationSpeed = rotationSpeed;
		    _scaleValue = scale;
            _scale = new Vector2(scale, scale);

			_velocity = new Vector2((float)(speed * Math.Cos(angle)), (float)(speed * Math.Sin(angle)));

			_texture = null;
			_color = Color.White;
		    _origin = Vector2.Zero;

			_bounds = bounds;
		}

		protected Texture2D Texture => _texture;

		public Vector2 Position => _position;
		public Vector2 Velocity => _velocity;  //new
	    public Rectangle Rectangle => _rectangle;

	    public Vector2 Origin => _origin;

		public bool Collided { get; private set; }

		public void LoadContent(ContentManager content, GraphicsDevice graphicsDevice, string assetName)
		{
			_texture = content.Load<Texture2D>(assetName);

			OnContentLoaded(content, graphicsDevice);
		}

		protected virtual void OnContentLoaded(ContentManager content, GraphicsDevice graphicsDevice)
		{
            _origin = new Vector2(_texture.Width / 2.0f, _texture.Height / 2.0f);

            UpdateTransformMatrix();
			UpdateRectangle();
		}

	    private void UpdateTransformMatrix()
	    {
	        //SRT = Reverse Origin * Scale * Rotation * Translation
	        _transform = Matrix.CreateTranslation(new Vector3(-_origin, 0)) *
	                     Matrix.CreateScale(_scaleValue) *
	                     Matrix.CreateRotationZ(_angle) *
	                     Matrix.CreateTranslation(new Vector3(_position, 0));
	    }

		private void UpdateRectangle()
		{
		    Vector2 topLeft = Vector2.Transform(Vector2.Zero, _transform);
		    Vector2 topRight = Vector2.Transform(new Vector2(_texture.Width, 0), _transform);
		    Vector2 bottomLeft = Vector2.Transform(new Vector2(0, _texture.Height), _transform);
		    Vector2 bottomRight = Vector2.Transform(new Vector2(_texture.Width, _texture.Height), _transform);

            Vector2 min = new Vector2(MathEx.Min(topLeft.X, topRight.X, bottomLeft.X, bottomRight.X), 
                MathEx.Min(topLeft.Y, topRight.Y, bottomLeft.Y, bottomRight.Y));

            Vector2 max = new Vector2(MathEx.Max(topLeft.X, topRight.X, bottomLeft.X, bottomRight.X),
                MathEx.Max(topLeft.Y, topRight.Y, bottomLeft.Y, bottomRight.Y));

            _rectangle = new Rectangle((int)min.X, (int)min.Y, (int)(max.X - min.X), (int)(max.Y - min.Y));
        }

		public virtual void Unload()
		{
			_texture.Dispose();
		}

		public void Update(GameTime gameTime)
		{
			_position += _velocity * (float)gameTime.ElapsedGameTime.TotalSeconds;

		    UpdateRotation(gameTime);
            UpdateTransformMatrix();
			UpdateRectangle();

			CheckBounds();
		}

	    private void UpdateRotation(GameTime gameTime)
	    {
	        _angle += (float) (_rotationSpeed * gameTime.ElapsedGameTime.TotalSeconds);

	        if (_angle < 0)
	        {
	            _angle = MathHelper.TwoPi - Math.Abs(_angle);
	        }

            else if (_angle > MathHelper.TwoPi)
            {
                _angle = _angle - MathHelper.TwoPi;
            }
	    }

		private void CheckBounds()
		{
			if (_bounds == null) return;

			Vector2 change = Vector2.Zero;
			
			if (_rectangle.Left <= _bounds.Value.X)
			{
				change.X = _bounds.Value.X - _rectangle.Left;
			}
			else if (_rectangle.Right >= _bounds.Value.Right)
			{
				change.X = _bounds.Value.Right - _rectangle.Right;
			}

			if (_rectangle.Top <= _bounds.Value.Y)
			{
				change.Y = _bounds.Value.Y - _rectangle.Top;
			}
			else if (_rectangle.Bottom >= _bounds.Value.Bottom)
			{
				change.Y = _bounds.Value.Bottom - _rectangle.Bottom;
			}
			
			if (change == Vector2.Zero) return;

			_position = new Vector2((int) _position.X + change.X, (int) _position.Y + change.Y);
			UpdateRectangle();
		}

		public bool Collision(Sprite target)
		{
			Vector2 normal;
			var intersects = VectorReflection.Collision.RectangularCollision(this, target, out normal);

            Collided = intersects;
			target.Collided = intersects;

			if (intersects)
			{
				_velocity = _velocity.Reflect(normal);
			}

			return intersects;
		}

		public virtual void Draw(SpriteBatch spriteBatch, GameTime gameTime)
		{
			spriteBatch.Draw(_texture, _position, null, null, _origin, _angle, _scale, _color);
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VectorReflection
{
	public enum RectangleCollisionPoint
	{
		None,
		Top,
		Left,
		Bottom,
		Right,
		TopLeftCorner,
		TopRightCorner,
		BottomLeftCorner,
		BottomRightCorner
	}

	public static class RectangleCollisionPointEx
	{
		public static RectangleCollisionPoint Combine(this RectangleCollisionPoint source,
			RectangleCollisionPoint target)
		{
			if ((source == RectangleCollisionPoint.Left && target == RectangleCollisionPoint.Top) ||
				(source == RectangleCollisionPoint.Top && target == RectangleCollisionPoint.Left))
			{
				return RectangleCollisionPoint.TopLeftCorner;
			}

			if ((source == RectangleCollisionPoint.Right && target == RectangleCollisionPoint.Top) ||
				(source == RectangleCollisionPoint.Top && target == RectangleCollisionPoint.Right))
			{
				return RectangleCollisionPoint.TopRightCorner;
			}

			if ((source == RectangleCollisionPoint.Left && target == RectangleCollisionPoint.Bottom) ||
				(source == RectangleCollisionPoint.Bottom && target == RectangleCollisionPoint.Left))
			{
				return RectangleCollisionPoint.BottomLeftCorner;
			}

			if ((source == RectangleCollisionPoint.Right && target == RectangleCollisionPoint.Bottom) ||
				(source == RectangleCollisionPoint.Bottom && target == RectangleCollisionPoint.Right))
			{
				return RectangleCollisionPoint.BottomRightCorner;
			}

			return RectangleCollisionPoint.None;
		}
	}
}

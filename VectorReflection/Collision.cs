﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace VectorReflection
{
	public static class Collision
	{
		public static bool RectangularCollision(Sprite a, Sprite b)
		{
			return RectangularCollision(a.Rectangle, b.Rectangle);
		}

		public static bool RectangularCollision(Sprite a, Sprite b, out Vector2 normal)
		{
			normal = Vector2.Zero;
			if (!RectangularCollision(a.Rectangle, b.Rectangle)) return false;

			Vector2 delta = a.Position - b.Position;
			float absDeltax = Math.Abs(delta.X);
			float absDeltay = Math.Abs(delta.Y);

			//Right side
			if (delta.X > 0 && absDeltay < b.Rectangle.Height / 2.0 && a.Velocity.X < 0) normal = new Vector2(1, 0);

			//Left side
			if (delta.X < 0 && absDeltay < b.Rectangle.Height / 2.0 && a.Velocity.X > 0) normal = new Vector2(-1, 0);

			//Top side
			if (delta.Y < 0 && absDeltax < b.Rectangle.Width / 2.0 && a.Velocity.Y > 0) normal = new Vector2(0, -1);

			//Bottom side
			if (delta.Y > 0 && absDeltax < b.Rectangle.Width / 2.0 && a.Velocity.Y < 0) normal = new Vector2(0, 1);

			//Bottom Right
			var epsilon = 0.1;
			if ((Math.Abs(delta.X - -b.Rectangle.Width / 2.0) < epsilon && Math.Abs(delta.Y - b.Rectangle.Height / 2.0) < epsilon) ||
				(Math.Abs(delta.X - b.Rectangle.Width / 2.0) < epsilon && Math.Abs(delta.Y - b.Rectangle.Height / 2.0) < epsilon) ||
				(Math.Abs(delta.X - -b.Rectangle.Width / 2.0) < epsilon && Math.Abs(delta.Y - -b.Rectangle.Height / 2.0) < epsilon) ||
				(Math.Abs(delta.X - b.Rectangle.Width / 2.0) < epsilon && Math.Abs(delta.Y - -b.Rectangle.Height / 2.0) < epsilon))
			{
				normal = new Vector2(delta.X, delta.Y);
				normal.Normalize();
			}

			return true;
		}

		public static bool RectangularCollision(Rectangle a, Rectangle b)
		{
			return a.Intersects(b);
		}

		public static Vector2 GetNormal(RectangleCollisionPoint point, bool inside = false)
		{
			switch (point)
			{
				case RectangleCollisionPoint.Top:
					return new Vector2(0, inside ? 1 : -1);
				case RectangleCollisionPoint.Left:
					return new Vector2(inside ? 1 : -1, 0);
				case RectangleCollisionPoint.Bottom:
					return new Vector2(0, inside ? -1 : 1);
				case RectangleCollisionPoint.Right:
					return new Vector2(inside ? -1 : 1, 0);
				case RectangleCollisionPoint.TopLeftCorner:
					return GetCornerNormal(new Vector2(inside ? -1 : 1, inside ? -1 : 1));
				case RectangleCollisionPoint.TopRightCorner:
					return GetCornerNormal(new Vector2(inside ? 1 : -1, inside ? -1 : 1));
				case RectangleCollisionPoint.BottomLeftCorner:
					return GetCornerNormal(new Vector2(inside ? -1 : 1, inside ? 1 : -1));
				case RectangleCollisionPoint.BottomRightCorner:
					return GetCornerNormal(new Vector2(inside ? 1 : -1, inside ? 1 : -1));
				default:
					return Vector2.Zero;
			}
		}

		public static Vector2 GetCornerNormal(Vector2 delta)
		{
			Vector2 n = delta;
			n.Normalize();

			return n;
		}
	}
}
